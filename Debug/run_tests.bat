@ECHO OFF
setlocal enabledelayedexpansion

SET /A all_count=0
SET /A ok_count=0

FOR /D %%I IN ("Tests/*") DO (
    FOR %%J IN ("Tests/"%%I"/*") DO (
        SET /A all_count+=1
        CALL Compiler.exe /l  Tests/%%I/%%J > output.txt
        FC output.txt Tests/%%I/output/%%J >nul 2>&1
        IF errorlevel 1 (
            ECHO Error in test %%I/%%J
        ) ELSE (
            SET /A ok_count+=1
            ECHO .
        )
    )
)
ECHO %all_count% test(s) ran.
ECHO %ok_count% test(s) passed.