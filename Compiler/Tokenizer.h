#pragma once
#include "Token.h"
#include <stdio.h>
#include <string>
#include <iostream>



class Tokenizer
{

private:
	FILE *file_input;
	Token token;
	string str;
	string::iterator iterator;
	Position pos;
	int columnOffset;
	bool isTokens;

	bool can_number_lexem(const string::iterator it);
	bool can_reserved_words(const string::iterator it);
	void new_string();
	void find_next();

	void set_token(const Position pos, const string source, const int length, void *value, const pascal_tokens type);
	void set_decimal_number(const Position pos);
	void set_comment(const Position Pos);
	void set_string(const Position Pos);
	void set_hex_number(const Position Pos);

public:
	Tokenizer(const char* filename);

	void next();
	const Token current();
	void print_token();
	void check_type(const pascal_tokens PT);
	const bool is_token();


};

