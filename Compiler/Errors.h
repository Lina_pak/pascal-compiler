#include <string>

using namespace std;

typedef struct Position {
	int line, column;
	Position(const int l = 0, const int c = 0) : line(l), column(c) {};
} Position;

static string string_position(const Position pos) {
	return "Line " + to_string(pos.line + 1) + " Column " + to_string(pos.column + 1);
}

class Error {
public:
	Error(const string err_msg, Position Pos) : error_msg(err_msg) {}

	Position pos;
	string error_msg;
};

class UndefinedError : public Error {
public:
	UndefinedError(Position Pos) : Error("Error: Undefined error. " + string_position(Pos), Pos) {}
};

class BadChar : public Error {
public:
	BadChar(Position Pos) :
		Error("Unknown Symbol in " + string_position(Pos), Pos) {}
};

class BadNL : public Error {
public:
	BadNL(Position Pos) : Error("Incorrect Symbol in " + string_position(Pos), Pos) {}
};

class BadEOF : public Error {
public:
	BadEOF(Position Pos) : Error("Unexpected EOF in " + string_position(Pos), Pos) {};
};