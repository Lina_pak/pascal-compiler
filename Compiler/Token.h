#pragma once


#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <iomanip>
#include "Errors.h"

using namespace std;

const char separator = ' ';
const int  posWidth = 5;
const int  classWidth = 15;
const int  valueWidth = 15;
const int textWidth = 40;

enum pascal_tokens {
	TK_AND, TK_ARRAY, TK_BEGIN, TK_BOOLEAN, TK_BREAK,
	TK_CASE, TK_CONST, TK_CONTINUE,
	TK_DIV_INT, TK_DOWNTO, TK_DO,
	TK_EXCEPT, TK_ELSE, TK_END, TK_EXIT,
	TK_FALSE, TK_FINALLY, TK_FORWARD, TK_FOR, TK_FUNCTION,
	TK_GOTO, TK_IF,
	TK_LABEL, TK_MOD, TK_NIL, TK_NOT,
	TK_OF, TK_OR, TK_OUT, TK_PROCEDURE, TK_PROGRAM,
	TK_RAISE, TK_RECORD, TK_REPEAT,
	TK_SHL, TK_SHR, TK_STRING,
	TK_THEN, TK_TO, TK_TRUE, TK_TRY, TK_TYPE,
	TK_UNTIL, TK_VAR, TK_WHILE, TK_XOR,

	TK_NOT_EQUAL, TK_LESS_EQUAL, TK_GREAT_EQUAL, TK_ASSIGNED,
	TK_PLUS_EQUAL, TK_SUB_EQUAL, TK_MUL_EQUAL, TK_DIV_EQUAL,

	TK_PLUS, TK_MINUS, TK_MUL, TK_DIV, TK_EQUAL,
	TK_LESS, TK_GREAT, TK_OPEN_SQUARE_BRACKET, TK_CLOSE_SQUARE_BRACKET, TK_DOUBLE_POINT,
	TK_POINT, TK_COMMA, TK_OPEN_BRACKET, TK_CLOSE_BRACKET, TK_COLON,
	TK_CAP, TK_DOG, TK_OPEN_BRACE, TK_CLOSE_BRACE, TK_DOLLAR,
	TK_GRILL, TK_AMPERSAND, TK_PERCENT, TK_SEMICOLON,

	TK_IDENTIFIER, TK_REAL_VALUE, TK_INTEGER_VALUE, TK_STRING_VALUE, TK_HEX_VALUE,
	NOT_TOKEN
};




class Token
{
private:
	void set(const Position pos, const string source, void *value, const pascal_tokens type);
public:
	Token() : pos(0, 0), source(), type(NOT_TOKEN) {};
	friend class Tokenizer;
	pascal_tokens type;
	string source;
	Position pos;
	void *value;

	void print();


};

