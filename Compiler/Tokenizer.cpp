#include "Tokenizer.h"
#include <fstream>
#include <iostream>
#include <stdio.h>

static const char *const tokens_str[] = {
	"and", "array", "begin", "boolean", "break",
	"case", "const", "continue",
	"div", "downto", "do",
	"except", "else", "end", "exit",
	"false", "finally", "forward", "for", "function",
	"goto", "if",
	"label", "mod", "nil", "not",
	"of", "or", "out", "procedure", "program",
	"raise", "record", "repeat",
	"shl", "shr", "string",
	"then", "to", "true", "try", "type",
	"until", "var", "while", "xor",

	"<>", "<=", ">=", ":=",
	"+=", "-=", "*=", "/=",

	"+", "-", "*", "/", "=",
	"<", ">", "[", "]", "..",
	".", ",", "(", ")", ":",
	"^", "@", "{", "}", "$",
	"#", "&", "%", ";",

	"identifier", "double_value", "integer_value", "string_value", "hex_value"
};

pascal_tokens reserved_words[] = {
	TK_AND, TK_ARRAY, TK_BEGIN, TK_BOOLEAN, TK_BREAK, TK_CASE, TK_CONST, TK_CONTINUE, TK_DIV_INT, TK_DOWNTO, 
	TK_DO, TK_EXCEPT, TK_ELSE, TK_END, TK_EXIT, TK_FALSE, TK_FINALLY, TK_FOR, TK_FORWARD, TK_FUNCTION, TK_GOTO, 
	TK_IF, TK_LABEL, TK_MOD, TK_NIL, TK_NOT, TK_OF, TK_OR, TK_OUT, TK_PROCEDURE, TK_PROGRAM, TK_RAISE, TK_RECORD,
	TK_REPEAT, TK_SHL, TK_SHR, TK_STRING, TK_THEN, TK_TO, TK_TRUE, TK_TRY, TK_TYPE, TK_UNTIL, TK_VAR, TK_WHILE, TK_XOR
};

pascal_tokens operators[] = {
	TK_NOT_EQUAL, TK_LESS_EQUAL, TK_GREAT_EQUAL, TK_ASSIGNED, TK_PLUS_EQUAL, TK_SUB_EQUAL, TK_MUL_EQUAL,
	TK_DIV_EQUAL, TK_PLUS, TK_MINUS, TK_MUL, TK_DIV, TK_EQUAL, TK_LESS, TK_GREAT, TK_OPEN_SQUARE_BRACKET,
	TK_CLOSE_SQUARE_BRACKET, TK_DOUBLE_POINT, TK_POINT, TK_COMMA, TK_OPEN_BRACKET, TK_CLOSE_BRACKET, 
	TK_COLON, TK_CAP, TK_DOG, TK_OPEN_BRACE, TK_CLOSE_BRACE, TK_DOLLAR, TK_GRILL, TK_AMPERSAND, TK_PERCENT, TK_SEMICOLON
};


bool Tokenizer::can_number_lexem(const string::iterator it)
{
	char c = *it;
	return !((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_');
}

bool Tokenizer::can_reserved_words(const string::iterator it)
{
	char c = *it;
	return !((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_') || (c >= '0' && c <= '9'));
}

void Tokenizer::new_string()
{
	if (str.length() > 0 && *str.crbegin() == EOF) {
		isTokens = false;
		return;
	}

	string newStr;

	do newStr += getc(file_input);
	while (*newStr.crbegin() != EOF && *newStr.crbegin() != '\n');

	str.assign(newStr);
	this->iterator = str.begin();
	pos.line++;
	pos.column = 0;
	columnOffset = 0;
}

void Tokenizer::find_next()
{
	pos.column = iterator - str.cbegin() + columnOffset;

	if ((*iterator < 0 || *iterator > 127) && (*iterator != EOF)) {
		//throw (Bad_cc(pos));
		int i = 1;
	}


	//if there is tabulation or space
	while (*iterator == ' ' || *iterator == '\t') { 
		if (*iterator == ' ') {
			pos.column++;
		}
		else if (*iterator == '\t') {
			pos.column += 4;
			columnOffset += 3;
		}
		iterator++;
	}

	// set float or integer value 
	if (*iterator >= '0' && *iterator <= '9') {
		set_decimal_number(pos); 
		return;
	}
	else if (*iterator == '$') {
		set_hex_number(pos); /* set hex value */
		return;
	}
	else if (*iterator == '\n') {
		set_token(pos, "", 0, 0, NOT_TOKEN); // end string 
		this->new_string();
		return;
	}
	else if (*iterator == EOF) {
		set_token(pos, str.substr(iterator - str.cbegin(), 1), 1, nullptr, NOT_TOKEN); // end file 
		isTokens = false;
		return;
	}
	else if (*iterator == '{') {// cut comment 
		set_comment(pos); 
		return;
	}
	else if (str.cend() - iterator > 2 && *iterator == '/' && *(iterator + 1) == '/') {
		set_token(pos, "", 0, nullptr, NOT_TOKEN); /* cut comment */
		this->new_string();
		return;
	}
	else if (*iterator == '\'') {
		set_string(pos); /* set string value */
		return;
	}
		


	for (int i = 0; i < sizeof(reserved_words) / sizeof(pascal_tokens); ++i) { // Other Tokens from array 'Special_Symbols' 
		int len = strlen(tokens_str[reserved_words[i]]);
		const char* str1 = str.substr(iterator - str.cbegin(), len).c_str();
		if (_strnicmp(str.substr(iterator - str.cbegin(), len).c_str(), tokens_str[reserved_words[i]], len) == 0 && can_reserved_words(iterator + len)) {
			string tmp_rw = str.substr(iterator - str.cbegin(), len);
			set_token(pos, tmp_rw, len, (void*)tmp_rw.c_str(), reserved_words[i]);
			return;
		}
	}

	for (int i = 0; i < sizeof(operators) / sizeof(pascal_tokens); ++i) { // Other Tokens from array 'Operators' 
		int len = strlen(tokens_str[operators[i]]);
		if (str.compare(iterator - str.cbegin(), len, tokens_str[operators[i]]) == 0) {
			string tmp_op = str.substr(iterator - str.cbegin(), len);
			set_token(pos, tmp_op, len, (void*)tmp_op.c_str(), operators[i]);
			return;
		}
	}

	string::iterator it_count = iterator + 1; // set identifier or error 
	while (*it_count >= 'a' && *it_count <= 'z' || *it_count >= 'A' && *it_count <= 'Z' || *it_count >= '0' && *it_count <= '9' || *it_count == '_') {
		++it_count;
	}

	string tmp_str = str.substr(iterator - str.cbegin(), it_count - iterator);
	set_token(pos, tmp_str, it_count - iterator, (void*)tmp_str.c_str(), TK_IDENTIFIER); 

}


void Tokenizer::set_token(const Position pos, const string source, const int length, void *value, const pascal_tokens type)
{
	token.set(pos, source, value, type);
	iterator += length;
}

void Tokenizer::set_decimal_number(const Position pos)
{
	string::iterator it_count = iterator + 1;

	while ((*it_count >= '0') && (*it_count <= '9'))
		it_count++;

	/*float number*/
	if (*it_count == '.' || tolower(*it_count == 'e')) {
		if ((*it_count == '.') && ((tolower(*(it_count + 1)) != 'e') || ((tolower(*(it_count + 2)) != 'e') && ((*(it_count+1) >= '0') && (*(it_count + 1) <= '9'))))){ /*a.b*/
			it_count++;
			if (*it_count < '0' || *it_count > '9') {
				//throw NoFract(pos)
			}
		}
		else { /*ae+b*/
			if ((tolower(*it_count) == 'e') && ((*(it_count + 1) == '-') || (*(it_count + 1) == '+'))) {
				it_count++;
			}
			else if (*it_count == '.') {
				do it_count++;
				while (*it_count >= '0' && *it_count <= '9');
				if (tolower(*it_count) == 'e')
					it_count++;
			}
		}
		
		do it_count++;
		while (*it_count >= '0' && *it_count <= '9');

		string tmp = str.substr(iterator - str.cbegin(), it_count - iterator);
		long double real_num = stof(tmp, nullptr);
		set_token(pos, tmp, it_count - iterator, static_cast<void*> (&real_num), TK_REAL_VALUE);
	}

	else { /*integer*/
		if (!can_number_lexem(it_count)) {
			Position curr_pos;
			curr_pos.line = pos.line;
			curr_pos.column = it_count - str.cbegin();
			throw BadChar(curr_pos);
		}
		string tmp = str.substr(iterator - str.cbegin(), it_count - iterator);
		int int_num = stof(tmp, nullptr);
		set_token(pos, tmp, it_count - iterator, static_cast<void*> (&int_num), TK_INTEGER_VALUE);
	}
}

void Tokenizer::set_comment(const Position Pos)
{
	string::iterator it_count = iterator + 1;
	int openBraceCount = 1;
	while (openBraceCount > 0) {
		if (*it_count == EOF) {
			throw BadEOF(Pos.line);
		}
		if (*it_count == '}') {
			openBraceCount--;
		}
		if (*it_count == '{') {
			openBraceCount++;
		}
		if (*it_count == '\n') {
			new_string();
			it_count = str.begin();
			continue;
		}
		it_count++;
	}
	set_token(pos, str.substr(iterator - str.cbegin(), it_count - iterator), it_count - iterator, nullptr, NOT_TOKEN);
}

void Tokenizer::set_string(const Position Pos)
{
	string::iterator it_count = iterator + 1;
	while (*it_count != '\'' && *it_count != '\n' && *it_count != EOF) {
		++it_count;
	}
	switch (*it_count) {
	case '\'':
	{
		string tmp_str = str.substr(iterator - str.cbegin() + 1, it_count - iterator - 1);
		set_token(Pos, tmp_str, it_count - iterator + 1, (void*)tmp_str.c_str(), TK_STRING_VALUE);
		break;
	}
		
	case '\n': throw BadNL(Pos);
	case EOF: throw BadEOF(Pos.line);
	}
}

void Tokenizer::set_hex_number(const Position Pos)
{
	string::iterator it_count = iterator + 1;
	
	while ((*it_count >= '0' && *it_count <= '9') || (*it_count) >= 'a' && tolower(*it_count) <= 'f' || ((*it_count) >= 'A' && tolower(*it_count) <= 'F')) {
		++it_count;
	}
	if (it_count - iterator == 1 || !can_number_lexem(it_count)) {
		throw BadChar(Pos);
	}

	string tmp_str = str.substr(iterator - str.cbegin() + 1, it_count - iterator - 1);
	tmp_str[0] = 'x';
	tmp_str = '0' + tmp_str;
	int int_num = stof(tmp_str, nullptr);
	set_token(Pos, str.substr(iterator - str.cbegin() + 1, it_count - iterator - 1), it_count - iterator, static_cast<void*> (&int_num), TK_HEX_VALUE);
}

Tokenizer::Tokenizer(const char* filename) : token(), file_input(fopen(filename, "r")), isTokens(true), pos(-1, -1)
{
	new_string();
}

void Tokenizer::next()
{
	do find_next(); 
	while (isTokens && token.type == NOT_TOKEN);
}

const Token Tokenizer::current()
{
	return Token(token);
}

const bool Tokenizer::is_token()
{
	return isTokens;
}


void Tokenizer::print_token()
{

	if (token.type != NOT_TOKEN) {
		cout << left << setw(posWidth) << setfill(separator) << token.pos.line + 1;
		cout << left << setw(posWidth) << setfill(separator) << token.pos.column + 1;
		cout << left << setw(valueWidth) << setfill(separator) << token.source.c_str();
		switch (token.type) 
		{
		case TK_REAL_VALUE:
		{
			cout << left << setw(valueWidth) << setfill(separator) << *static_cast<long double*> (token.value);
			break;
		}
		case TK_INTEGER_VALUE:
		{
			cout << left << setw(valueWidth) << setfill(separator) << *static_cast<int*> (token.value);
			break;
		}

		case TK_IDENTIFIER:
		{
			cout << left << setw(valueWidth) << setfill(separator) << token.source;
			break;
		}

		case TK_STRING_VALUE:
		{
			cout << left << setw(valueWidth) << setfill(separator) << token.source;
			break;
		}

		case TK_HEX_VALUE:
		{
			cout << left << setw(valueWidth) << setfill(separator) << *static_cast<int*> (token.value);
			break;
		}

		default:
		{
			cout << left << setw(valueWidth) << setfill(separator) << token.source;
			break;
		}


		}
		//return NOT_TOKEN;
		cout << left << setw(valueWidth) << setfill(separator) << tokens_str[token.type];
		cout << endl;
	}

		
}


		//cout << token.pos.line + 1 << "\t" << token.pos.column + 1 << "\t" << tokens_str[token.type] << "\t" << token.value.c_str() << endl;

void Tokenizer::check_type(const pascal_tokens PT)
{
	if (token.type != PT) {
		//throw UnexpectedSymbol(...)
	}
}
