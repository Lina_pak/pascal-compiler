
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "Tokenizer.h"



using namespace std;

#define print_format \
	cout << "\n" << "Usage: Compiler /l, /h" << endl << endl; 

int main(int argc, char **argv) {

	if (argc == 1) {
		print_format;
	}

	if (argc == 2) {
		if (strcmp(argv[1], "/l") == 0) {
			cout << "Add an input file to require" << "\n" << endl;
			return 0;
		}
		if (strcmp(argv[1], "/h") == 0) {
			print_format;
			return 0;
		}
	}

	try {
		if (argc == 3) {
			if (strcmp(argv[1], "/l") == 0) {
				Tokenizer Tknzr(argv[2]);
				while (Tknzr.is_token())
				{
					Tknzr.next();
					Tknzr.print_token();
				}
			}
		}
	}
	catch (Error err) {
		cout << err.error_msg << endl;
	}
	

	//system("pause");
}