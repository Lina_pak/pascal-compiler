#include "Token.h"








void Token::set(const Position pos, const string source, void* value, const pascal_tokens type)
{
	this->pos.line = pos.line;
	this->pos.column = pos.column;
	this->source = source;
	//this->value = value;
	switch (type)
	{
	case TK_INTEGER_VALUE:
	{
		this->value = new int;
		memcpy(this->value, value, sizeof(int));
		break;
	}
	case TK_HEX_VALUE:
	{
		this->value = new int;
		memcpy(this->value, value, sizeof(int));
		break;
	}
	case TK_REAL_VALUE: 
	{
		this->value = new float;
		memcpy(this->value, value, sizeof(long double));
		break;
	}
	default:
		this->value = value;
		break;
	}
	this->type = type;
}

void Token::print()
{
	cout << left << setw(posWidth) << setfill(separator) << pos.line;
	cout << left << setw(classWidth) << setfill(separator) << pos.column;
	cout << left << setw(valueWidth) << setfill(separator) << source;
	cout << left << setw(valueWidth) << setfill(separator) << type;
	cout << endl; 
}


